/**
 * Copyright (c) 2018  almedso GmbH
 * 
 * SPDX-License-Identifier: CC-BY-SA-2.5
 */

bool is_fibonacci(unsigned int n)
{
  int a = 1;
  int next = a;
  while ( a <= n ) {
    if ( a == n ) return true;
    unsigned int tmp = next;
    next = a + next; a = tmp; 
  }
  return false;
}

#include "gtest/gtest.h"

namespace {

  class Test_IsFibonacci : public ::testing::Test {
  };

  TEST(Test_IsFibonacci, WrongNumbers) {
    ASSERT_FALSE(is_fibonacci(0)) << "0 is not a fibonacci number";
    ASSERT_FALSE(is_fibonacci(4)) << "4 is not a fibonacci number";
    ASSERT_FALSE(is_fibonacci(100)) << "100 is not a fibonacci number";
  }

  TEST(Test_IsFibonacci, CorrectNumbers) {
    ASSERT_TRUE(is_fibonacci(1)) << "1 is a fibonacci number";
    ASSERT_TRUE(is_fibonacci(2)) << "2 is a fibonacci number";
    ASSERT_TRUE(is_fibonacci(3)) << "3 is a fibonacci number";
    ASSERT_TRUE(is_fibonacci(5)) << "5 is a fibonacci number";
    ASSERT_TRUE(is_fibonacci(8)) << "8 is a fibonacci number";
    ASSERT_TRUE(is_fibonacci(13)) << "13 is a fibonacci number";
  }

}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
