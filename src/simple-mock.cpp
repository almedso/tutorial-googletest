/**
 * Copyright (c) 2018  almedso GmbH
 * 
 * SPDX-License-Identifier: CC-BY-SA-2.5
 */

/** Given an Entity (just the dependency injection) */
class Entity {
  protected:
    int _a; 
  public:
    virtual ~Entity() {}
    virtual void setVal(int a) { _a = a; }
    virtual int getVal() const { return _a; }
};

/** Given a Doer (our subject under test) */
class Doer {
  private: 
    Entity& _e;
  public:
    Doer(Entity& a) : _e(a) {}
    void do_something(int x) { _e.setVal(x); }
    int ask_something() const { return _e.getVal(); }
};



/** here comes the mock */
#include <gmock/gmock.h>

class MockEntity : public Entity {
  public:
    MOCK_METHOD1(setVal, void(int a));
    MOCK_CONST_METHOD0(getVal, int());
};


/** finally we do our test */
#include <gtest/gtest.h>

using ::testing::Return;

TEST(Test_Doer, setValOfMockIsCalled) {
  MockEntity mockEntity;  // establish a spy
  Doer doer(mockEntity);  // inject testing dependency
  EXPECT_CALL(mockEntity, setVal(5)).Times(1);  // formulate the expectation
  doer.do_something(5);  // actually invoke the subject under test
}

TEST(Test_Doer, getValOfMockIsCalled) {
  MockEntity mockEntity;  // establish a stub
  Doer doer(mockEntity);  // inject testing dependency
  EXPECT_CALL(mockEntity, getVal()).Times(1)
    .WillOnce(Return(4));  // formulate the expectation
  ASSERT_EQ(4, doer.ask_something());  // actually invoke the subject under test
}




