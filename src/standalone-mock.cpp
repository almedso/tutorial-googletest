/**
 * Copyright (c) 2018  almedso GmbH
 * 
 * SPDX-License-Identifier: CC-BY-SA-2.5
 */

/** Given an Entity (just the dependency injection) */
class Entity {
  protected:
    int _a; 
  public:
    virtual ~Entity() {}
    void setVal(int a) { _a = a; }
    int getVal() const { return _a; }
};

/** Given a Doer (our subject under test) */
template <class E>
class DoerImpl {
  private: 
    E& _e;
  public:
    DoerImpl(E& a) : _e(a) {}
    virtual ~DoerImpl() {}
    void do_something(int x) { _e.setVal(x); }
    int ask_something() const { return _e.getVal(); }
};

static Entity se;

class Doer: public DoerImpl<Entity> {
  public:
    Doer() : DoerImpl<Entity>(se) {}
};


/** here comes the mock */
#include <gmock/gmock.h>

class MockEntity {
  public:
    MOCK_METHOD1(setVal, void(int a));
    MOCK_CONST_METHOD0(getVal, int());
};


/** finally we do our test */
#include <gtest/gtest.h>

using ::testing::Return;


TEST(Test_Doer, TestDoerAndEntityCollaboration) {
  Doer d;
  d.do_something( 5 );
  ASSERT_EQ( 5, d.ask_something() );
}

TEST(Test_Doer, setValOfStandaloneMockIsCalled) {
  MockEntity mockEntity;  // establish a spy
  DoerImpl<MockEntity> doer(mockEntity);  // inject testing dependency
  EXPECT_CALL(mockEntity, setVal(5)).Times(1);  // formulate the expectation
  doer.do_something(5);  // actually invoke the subject under test
}

TEST(Test_Doer, getValOfStandaloneMockIsCalled) {
  MockEntity mockEntity;  // establish a spy
  DoerImpl<MockEntity> doer(mockEntity);  // inject testing dependency
  EXPECT_CALL(mockEntity, getVal()).Times(1)
    .WillOnce(Return(4));  // formulate the expectation
  ASSERT_EQ(4, doer.ask_something());  // actually invoke the subject under test
}
