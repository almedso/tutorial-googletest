# README

this is a tutorial for google test on cpp / c.
implemented by sphinx / hieroglyph.

## Getting Started

Prerequisites:

* have python installed and sphinx installed
* alternatively use docker container **almedso/sphinx-doc-builder** and perform
  the following actions inside the container

Install hieroglyph:

```
pip install -r requirements.txt
```

Create the docu slides:

```
sphinx-build -b slides -c docs docs  _build/slides
```

You can also compile the sources like:

```
g++ src/standalone-mock.cpp -o test-runner -l gtest -l  pthread -l gtest_main -l gmock
```
##  Published documentation

The presentation might be published via gitlab pages at

https://almedso.gitlab.io/tutorial-googletest


## References

* https://pypi.org/project/hieroglyph/
* https://github.com/google/googletest

