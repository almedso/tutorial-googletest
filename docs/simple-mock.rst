Standalone Mock
===============

Dependency
==========

In der Datei *src/simple-mock.cpp* ist der Mock (mit GoogleMock)
implementiert.

siehe:
https://github.com/abseil/googletest/blob/master/googlemock/docs/ForDummies.md

Das **Subject under Test** enthält eine aktive Dependency, die nicht
Gegenstand der Unit Tests ist.


.. literalinclude:: ../src/simple-mock.cpp
   :language: c
   :lines: 18-25


Dependency
==========

Die Entity dependency ist wie folgt definiert:

.. literalinclude:: ../src/simple-mock.cpp
   :language: c
   :lines: 8-15


Mock
====

Die Methoden *setVal* und *getVal* sind public und virtuell, daher
sollte der zu erstellende Mock von Entity ableiten.

.. literalinclude:: ../src/simple-mock.cpp
   :language: c
   :lines: 32-36


Tests - Mock als Spy
====================

* Testen vom public interface des SuT
* Mock wird als Spion benutzt

.. literalinclude:: ../src/simple-mock.cpp
   :language: c
   :lines: 44-49


Tests - Mock als Stub
=====================

* Testen vom public interface des SuT
* Mock wird als Stub (Baumstumpf, Stummel) benutzt und liefert Werte

.. literalinclude:: ../src/simple-mock.cpp
   :language: c
   :lines: 51-57

