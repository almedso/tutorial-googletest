Standalone Mock
===============

Dependency
==========

In der Datei *src/standalone-mock.cpp* ist der "freistehende Mock" implementiert.


siehe: 
https://github.com/abseil/googletest/blob/master/googlemock/docs/CookBook.md#mocking-nonvirtual-methods

Das **Subject under Test** enthält eine aktive Dependency, die nicht
Gegenstand der Unit Tests ist.

.. literalinclude:: ../src/standalone-mock.cpp
   :language: c
   :lines: 8-15

Mock
====

Die Methoden *setVal* und *getVal* sind nicht virtuell, daher
darf der zu erstellende Mock nicht von Entity ableiten.

.. literalinclude:: ../src/standalone-mock.cpp
   :language: c
   :lines: 40-44


Subject under Test
==================

Benutzung von Templates und Verstecken in Implementierungsklasse.

.. literalinclude:: ../src/standalone-mock.cpp
   :language: c
   :lines: 18-34

Tests mit Mock
==============

Wie gehabt: Testet SuT Verhalten


.. literalinclude:: ../src/standalone-mock.cpp
   :language: c
   :lines: 59-72


Tests mit echter Dependency
===========================

Testet ob echtes Sut erzeugt werden (Integration von SuT und
seiner Dependency)


.. literalinclude:: ../src/standalone-mock.cpp
   :language: c
   :lines: 53-57




