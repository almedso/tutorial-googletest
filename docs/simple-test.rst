Einfaches Testbeispiel
======================

In der Datei *src/simple-test.cpp* sind lauffähig alle Grundkonzepte enthalten.

* **Subject under Test** ist der Testgegenstand (hier eine einfache Funktion)
* **Testbed** ist die Testumgebung aus das **SuT** den Tests unterzogen wird.
* **Test Suite** ist die Menge der **Test Cases** die für das **SuT** spezifiziert wurden
* **Test Case** ist ein einzelner Test mit konkretem Ziel und Namen


Subject under Test
==================

ist hier eine einfache Funktion, die testet ob eine Zahl

.. literalinclude:: ../src/simple-test.cpp
   :language: c
   :lines: 7-17

Bestandteil der Fibonacci Folge ist.


Testbed
=======

.. literalinclude:: ../src/simple-test.cpp
   :language: cpp
   :lines: 19-25,41

*Test_IsFibunacci* representiert das Testbed.


TestSuite, Tests
================

Menge der Tests bildet die Test Suite.

.. literalinclude:: ../src/simple-test.cpp
   :language: cpp
   :lines: 19,26-31

* *TestCases* in *googletest*  mit Schlüsselwort *TEST*
* hier ist ein Test *WrongNumbers* beschrieben, der ausschließlich
  die falsche Returnwerte für Nicht-Fibunacci-Zahlen testet.

Positive Tests
==============

Im **Test Case** können mehrere Überprüfungen zusammengefasst werden.

.. literalinclude:: ../src/simple-test.cpp
   :language: c
   :lines: 19,32-39

**Tests Cases** werden im selben *namespace* wie auch das **Testbed**
definiert.

.. note::

   *include* nicht vergessen.

Testrunner
==========

Ist immer gleichartig.

.. literalinclude:: ../src/simple-test.cpp
   :language: c
   :lines: 43-46

Ist immer gleichartig
*googletest/googlemock* bietet einen Testrunner, der auch entfallen kann,
wenn man zum Executable die Bibliothek *gtest_main* zusätzlich hinzugelinkt.

.. note::

   Auch hier: *include* nicht vergessen.
