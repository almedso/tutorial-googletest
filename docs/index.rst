====================
Google Test Tutorial
====================

.. include:: prerequisites.rst
.. include:: run-tests.rst
.. include:: simple-test.rst
.. include:: simple-mock.rst
.. include:: standalone-mock.rst

.. ifnotslides::

   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

