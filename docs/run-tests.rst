Ausführen von unit tests
========================

1. Compilieren des Executable, bestehend aus *Testrunner*, *TestBed* und *Tests* (test code)
   und *Subject under Test*.

2. Ausführen des Executable. Liefert den Bericht, welche Tests erfolgreich waren (passed) und welche
   Tests fehlschlugen (failed)

Compilieren des der Tests
=========================

Gibt Hinweise ob *Subject under Test* überhaupt compiliert

.. code::

   g++ src/simple-test.cpp -o test-runner -l gtest -l pthread
   # mit test runner
   g++ src/simple-test.cpp -o test-runner -l gmock_main -l gmock -l gtest -l pthread



Aussführen des Test-Executable
==============================

.. code::

    $ ./test-runner 
    [==========] Running 2 tests from 1 test case.
    [----------] Global test environment set-up.
    [----------] 2 tests from Test_IsFibonacci
    [ RUN      ] Test_IsFibonacci.WrongNumbers
    [       OK ] Test_IsFibonacci.WrongNumbers (0 ms)
    [ RUN      ] Test_IsFibonacci.CorrectNumbers
    [       OK ] Test_IsFibonacci.CorrectNumbers (0 ms)
    [----------] 2 tests from Test_IsFibonacci (0 ms total)

    [----------] Global test environment tear-down
    [==========] 2 tests from 1 test case ran. (0 ms total)
    [  PASSED  ] 2 tests.
    $

